<?php


namespace verwaltung\controller;

use Twig\Environment;
use verwaltung\factory\FactoryInterface;
use verwaltung\helpers\DependencyResolver;
use verwaltung\repository\RepositoryInterface;

class StudentController
{
    /**
     * @var RepositoryInterface
     */
    private $repository;

    /**
     * @var FactoryInterface
     */
    private $factory;

    /**
     * @var Environment
     */
    private $template;

    /**
     * LoginController constructor.
     * @param RepositoryInterface $repository
     * @param Environment $template
     * @param FactoryInterface $factory
     */
    public function __construct(
        RepositoryInterface $repository,
        FactoryInterface $factory,
        Environment $template
    ) {
        $this->repository = $repository;
        $this->factory = $factory;
        $this->template = $template;
    }

    /**
     * @return string
     */
    public function registerToLecture(): string
    {
        $student = $this->repository->findById($_SESSION['user_id']);

        $connection = DependencyResolver::getClass('db-connection');
        $lectureFactory = DependencyResolver::getClass('lecture-factory');
        $lectureRepository = DependencyResolver
            ::getClassWithDependencies('lecture-repository', [$connection, $lectureFactory]);

        $lecture = $lectureRepository->findById($_POST['lecture_id']);

        if ($student == null) {
            $student = $this->factory->create([
                'id' => $_SESSION['user_id'],
                'firstname' => $_SESSION['user_firstname'],
                'surname' => $_SESSION['user_surname'],
                'lectures' => $lecture
            ]);

            $this->repository->save($student);
        }

        $this->repository->saveLecture($student, $lecture);

        header('location: /overview');

    }
}