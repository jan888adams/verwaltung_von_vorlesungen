<?php


namespace verwaltung\controller;

use http\Exception\InvalidArgumentException;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use verwaltung\repository\RepositoryInterface;

/**
 * Class LoginController
 * @package verwaltung\controller
 */
class LoginController
{
    /**
     * @var RepositoryInterface
     */
    private $repository;

    /**
     * @var Environment
     */
    private $template;

    /**
     * LoginController constructor.
     * @param RepositoryInterface $repository
     * @param Environment $template
     */
    public function __construct(
        RepositoryInterface $repository,
        Environment $template
    ) {
        $this->repository = $repository;
        $this->template = $template;
    }

    /**
     * @return string|null
     */
    public function login(): ?string
    {
        $user = $this->repository->findByEmail($_POST['email']);

        $renderedTemplate = null;

        try {
            if ($user != null && password_verify($_POST['password'], $user->getPassword())) {

                $_SESSION['access_right'] = $user->getAccessRight();
                $_SESSION['user_id'] = $user->getId();
                $_SESSION['user_firstname'] = $user->getFirstname();
                $_SESSION['user_surname'] = $user->getSurname();

                $renderedTemplate = $this->template->render('login\form.html.twig', [
                    'message' => 'you log in successfully',
                    'access_right' => $user->getAccessRight()
                ]);
            } else {
                $renderedTemplate = $this->template->render('login\form.html.twig',
                    ['message' => 'password and email combination is not valid']);
            }
        } catch (LoaderError | RuntimeError | SyntaxError $e) {
            throw new InvalidArgumentException();
        }

        return $renderedTemplate;
    }

    /**
     * destroy the current session and redirect to login form
     */
    public function logout(): void
    {
        session_destroy();
        header('location:  /');
    }

    /**
     * @return string
     */
    public function showForm(): string
    {
        try {
            return $this->template->render('login\form.html.twig');
        } catch (LoaderError | RuntimeError | SyntaxError $e) {
            throw new InvalidArgumentException();
        }
    }
}