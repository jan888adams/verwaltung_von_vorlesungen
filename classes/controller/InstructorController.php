<?php

namespace verwaltung\controller;

use http\Exception\InvalidArgumentException;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use verwaltung\factory\FactoryInterface;
use verwaltung\helpers\SessionHandler;
use verwaltung\repository\RepositoryInterface;

/**
 * Class InstructorController
 */
class InstructorController
{
    /**
     * @var RepositoryInterface
     */
    private $repository;

    /**
     * @var FactoryInterface
     */
    private $factory;

    /**
     * @var Environment
     */
    private $template;

    /**
     * ViewController constructor.
     * @param RepositoryInterface $repository
     * @param FactoryInterface $factory
     * @param Environment $template
     */
    public function __construct
    (
        RepositoryInterface $repository,
        FactoryInterface $factory,
        Environment $template
    ) {
        $this->repository = $repository;
        $this->factory = $factory;
        $this->template = $template;
    }


    /**
     * @param array $params
     * @return string
     */
    public function addInstructor(array $params): string
    {
        $isProfessor = isset($params['isProfessor']) ? 1 : 0;

        $instructor = $this->factory->create([
            'id' => uniqid(),
            'firstname' => $params['firstname'],
            'surname' => $params['surname'],
            'is_professor' => $isProfessor,
        ]);


        if (!$this->repository->save($instructor)) {
            $message = 'saved';
        } else {
            $message = 'not saved';
        }

        return $this->showForm($message);
    }

    /**
     * @param string $message
     * @return string
     */
    public function showForm($message = ''): string
    {
        $accessRight = SessionHandler::getSession()['access_right'];

        try {
            if ($accessRight == 4) {
                $renderedTemplate = $this->template->render('instructor/form.html.twig',
                    [
                        'access_right' => $accessRight,
                        'message' => $message
                    ]);
            } else {
                $renderedTemplate = $this->template->render('404/404.html.twig',
                    ['access_right' => $accessRight]);
            }
        } catch (LoaderError | RuntimeError | SyntaxError $e) {
            throw new InvalidArgumentException();
        }

        return $renderedTemplate;
    }

    /**
     * @return string
     */
    public function showTable(): string
    {
        $accessRight = SessionHandler::getSession()['access_right'];

        try {
            if ($accessRight == 4) {
                $renderedTemplate = $this->template->render('instructor/table.html.twig', [
                    'instructors' => $this->repository->findAll(),
                    'access_right' => $accessRight
                ]);
            } else {
                $renderedTemplate = $this->template->render('404/404.html.twig',
                    ['access_right' => $accessRight]);
            }
        } catch (LoaderError | RuntimeError | SyntaxError $e) {
            throw new InvalidArgumentException();
        }

        return $renderedTemplate;
    }

    public function showInstructor(string $id): string
    {
        $accessRight = SessionHandler::getSession()['access_right'];
        try {
            if ($accessRight == 4) {
                $renderedTemplate = $this->template->render('instructor/detail.html.twig',
                    [
                        'instructor' => $this->repository->findById($id),
                        'access_right' => $_SESSION['access_right']
                    ]);
            } else {
                $renderedTemplate = $this->template->render('404/404.html.twig',
                    ['access_right' => $accessRight]);
            }
        } catch (LoaderError | RuntimeError | SyntaxError $e) {
            throw new InvalidArgumentException();
        }
        return $renderedTemplate;
    }

}