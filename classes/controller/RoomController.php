<?php


namespace verwaltung\controller;

use http\Exception\InvalidArgumentException;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use verwaltung\factory\FactoryInterface;
use verwaltung\helpers\SessionHandler;
use verwaltung\repository\RepositoryInterface;

class RoomController
{
    /**
     * @var RepositoryInterface
     */
    private $repository;

    /**
     * @var FactoryInterface
     */
    private $factory;

    /**
     * @var Environment
     */
    private $template;

    /**
     * RoomController constructor.
     * @param RepositoryInterface $repository
     * @param FactoryInterface $factory
     * @param Environment $template
     */
    public function __construct
    (
        RepositoryInterface $repository,
        FactoryInterface $factory,
        Environment $template
    ) {
        $this->repository = $repository;
        $this->factory = $factory;
        $this->template = $template;
    }

    /**
     * @param array $params
     * @return string
     */
    public function addRoom(array $params): string
    {
        $room = $this->factory->create([
            'id' => uniqid(),
            'name' => $params['name'],
            'number' => $params['number']
        ]);

        if (!$this->repository->save($room)) {
            $message = 'saved';
        } else {
            $message = 'not saved';
        }

        return $this->showForm($message);
    }

    /**
     * @param string $message
     * @return string
     */
    public function showForm(string $message = ''): string
    {
        $accessRight = SessionHandler::getSession()['access_right'];

        try {
            if ($accessRight == 4) {
                $renderedTemplate = $this->template->render('room/form.html.twig',
                    [
                        'access_right' => $accessRight,
                        'message' => $message
                    ]);
            } else {
                $renderedTemplate = $this->template->render('404/404.html.twig',
                    ['access_right' => $accessRight]);
            }
        } catch (LoaderError | RuntimeError | SyntaxError $e) {
            throw new InvalidArgumentException();
        }

        return $renderedTemplate;
    }

}