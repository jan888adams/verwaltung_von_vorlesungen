<?php

namespace verwaltung\controller;

use http\Exception\InvalidArgumentException;
use LectureFinder;
use ServiceFinderInterface;
use ServicePersistenceInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use verwaltung\database\MySqlConnection;
use verwaltung\factory\FactoryInterface;
use verwaltung\factory\InstructorFactory;
use verwaltung\factory\RoomFactory;
use verwaltung\helpers\DependencyResolver;
use verwaltung\helpers\SessionHandler;
use verwaltung\repository\InstructorRepository;
use verwaltung\repository\RepositoryInterface;
use verwaltung\repository\RoomRepository;

class LectureController
{
    /**
     * @var ServiceFinderInterface
     */
    private $lectureFinder;

    /**
     * @var ServicePersistenceInterface
     */
    private $lecturePersistence;

    /**
     * @var InstructorFactory
     */
    private $factory;

    /**
     * @var Environment
     */
    private $template;


    public function __construct(Environment $template)
    {
        $this->lectureFinder = DependencyResolver::getClass('lecture-finder');
        $this->lecturePersistence = DependencyResolver::getClass('persistence-service');
        $this->factory = DependencyResolver::getClass('lecture-factory');
        $this->template = $template;
    }

    /**
     * @param array $params
     * @return string
     */
    public function addLecture(array $params): string
    {
        $instructorRepository = new InstructorRepository(new MySqlConnection(), new InstructorFactory());
        $roomRepository = new RoomRepository(new MySqlConnection(), new RoomFactory());

        $lecture = $this->factory->create([
            'id' => uniqid(),
            'name' => $params['name'],
            'room' => $roomRepository->findById($params['rooms']),
            'instructor' => $instructorRepository->findById($params['instructors'])
        ]);

        if (!$this->lecturePersistence->persist($lecture)) {
            $message = 'saved';
        } else {
            $message = 'not saved';
        }

        return $this->showForm($message);
    }

    /**
     * @param string $message
     * @return string
     */
    public function showTable(string $message = ''): string
    {
        $lectures = $this->lectureFinder->findAll();
        $data = [];

        foreach ($lectures as $lecture) {
            $data[] = [
                'id' => $lecture->getId(),
                'name' => $lecture->getName(),
                'room' => $lecture->getRoom(),
                'instructor' => $lecture->getInstructor(),
                'message' => $message
            ];
        }

        $accessRight = SessionHandler::getSession()['access_right'];

        try {
            $renderedTemplate = $this->template->render('lecture/table.html.twig', [
                'lectures' => $data,
                'access_right' => $accessRight
            ]);
        } catch (LoaderError | RuntimeError | SyntaxError $e) {
            throw new InvalidArgumentException();
        }

        return $renderedTemplate;
    }

    /**
     * @param string $message
     * @return string
     */
    public function showForm(string $message = ''): string
    {
        $connection = DependencyResolver::getClass('db-connection');
        $roomFactory = DependencyResolver::getClass('room-factory');
        $instructorFactory = DependencyResolver::getClass('instructor-factory');
        $instructorRepository = DependencyResolver
            ::getClassWithDependencies('instructor-repository', [$connection, $instructorFactory]);
        $roomRepository = DependencyResolver
            ::getClassWithDependencies('room-repository', [$connection, $roomFactory]);

        $accessRight = SessionHandler::getSession()['access_right'];

        try {
            if ($accessRight == 4) {
                $renderedTemplate = $this->template->render('lecture/form.html.twig',
                    [
                        'instructors' => $instructorRepository->findAll(),
                        'rooms' => $roomRepository->findAll(),
                        'access_right' => $_SESSION['access_right'],
                        'message' => $message
                    ]
                );
            } else {
                $renderedTemplate = $this->template->render('404/404.html.twig');
            }
        } catch (LoaderError | RuntimeError | SyntaxError $e) {
            throw new InvalidArgumentException();
        }

        return $renderedTemplate;
    }

}