<?php

namespace verwaltung\controller;

use http\Exception\InvalidArgumentException;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use verwaltung\factory\FactoryInterface;
use verwaltung\repository\RepositoryInterface;

class UserController
{
    /**
     * @var RepositoryInterface
     */
    private $repository;

    /**
     * @var FactoryInterface
     */
    private $factory;

    /**
     * @var Environment
     */
    private $template;

    /**
     * UserController constructor.
     * @param RepositoryInterface $repository
     * @param FactoryInterface $factory
     * @param Environment $template
     */
    public function __construct(RepositoryInterface $repository, FactoryInterface $factory, Environment $template)
    {
        $this->repository = $repository;
        $this->factory = $factory;
        $this->template = $template;
    }

    /**
     * @return string
     */
    public function addUser(): string
    {
        $saltedHashedPassword = password_hash($_POST['password'], PASSWORD_DEFAULT);

        $user = $this->factory->create([
            'id' => uniqid(),
            'firstname' => $_POST['firstname'],
            'surname' => $_POST['surname'],
            'email' => $_POST['email'],
            'password' => $saltedHashedPassword,
            'access_right' => $_POST['access_right']
        ]);

        $this->repository->save($user);

        if(isset($_SESSION['access_right'])){
            $access_right = $_SESSION['access_right'];
        }else {
            $access_right = '';
        }

        try {
            return $this->template->render('user/form.html.twig',
                [
                    'message' => 'you have successfully registered',
                    'access_right' => $access_right
                ]);
        } catch (LoaderError | RuntimeError | SyntaxError $e) {
            throw new InvalidArgumentException();
        }
    }

    /**
     * @return string
     */
    public function showForm(): string
    {
        $accessRight = '';

        if (isset($_SESSION['access_right'])) {
            $accessRight = $_SESSION['access_right'];
        }
        try {
            return $this->template->render('user/form.html.twig', ['access_right' => $accessRight]);
        } catch (LoaderError | RuntimeError | SyntaxError $e) {
            throw new InvalidArgumentException();
        }
    }
}