<?php

namespace verwaltung\controller;

use InvalidArgumentException;
use ReflectionClass;
use ReflectionException;

/**
 * Class FrontController
 * @package verwaltung\controller
 */
class FrontController
{
    /**
     * @var string (controller)
     */
    private $controller;
    /**
     * @var array (function)
     */
    private $action = [];
    /**
     * @var array (dependencies needed for the controller)
     */
    private $dependencies = [];
    /**
     * @var array (parameters for the function call)
     */
    private $params = [];
    /**
     * @var array (urls with defined responses)
     */
    private $routes;

    /**
     * FrontController constructor.
     * @param array $routs
     */
    public function __construct(array $routs)
    {
        $this->routes = $routs;
        $this->getResponse();
    }

    /**
     * generate a response for the given server request.
     */
    private function getResponse(): void
    {
        $route = $_SERVER["REQUEST_URI"];

        if (strpos($route, '?') !== false) {
            $route = substr($route, 0, strpos($route, '?'));
        }

        $responses = $this->routes[$route];

        if (!empty($_POST)) {
            $response = $responses['post'];
        } elseif (!empty($_GET)) {
            $response = $responses['get'];
        } else {
            $response = $responses['http'];
        }

        if (!empty($response)) {
            if (isset($response["dependencies"])) {
                $this->dependencies = $response["dependencies"];
            }

            if (isset($response['controller'])) {
                $this->setController($response['controller']);
            }

            if (isset($response['action'])) {
                $this->setAction($response['action']);
            }

            if (isset($response['params'])) {
                $this->setParams($response['params']);
            }

            if (isset($responses['get'])) {
                $this->setParams([$_GET['p']]);
            }
        }
    }

    /**
     * @param $controller
     * @return $this
     */
    private function setController($controller): FrontController
    {
        if (!class_exists($controller)) {
            throw new InvalidArgumentException(
                'The action controller ' . $controller . 'has not been defined.');
        }
        $this->controller = $controller;
        return $this;
    }

    /**
     * @param $action
     * @return FrontController
     */
    private function setAction($action): FrontController
    {
        try {
            $reflector = new ReflectionClass($this->controller);
        } catch (ReflectionException $e) {
            throw new \http\Exception\InvalidArgumentException('can not invoke' . $this->controller);
        }

        if (!$reflector->hasMethod($action)) {
            throw new InvalidArgumentException(
                'The controller action' . $action . 'has been not defined.');
        }
        $this->action = $action;
        return $this;
    }

    /**
     * @param array $params
     * @return $this
     */
    private function setParams(array $params): FrontController
    {
        $this->params = $params;
        return $this;
    }

    /**
     * call the response for the server request
     */
    public function run(): void
    {
        try {
            $controllerClass = new ReflectionClass($this->controller);
          echo call_user_func_array([$controllerClass->newInstanceArgs($this->dependencies), $this->action],
                $this->params);
        } catch (ReflectionException $e) {
            throw new \http\Exception\InvalidArgumentException('can not call the response for the server request');
        }
    }

}