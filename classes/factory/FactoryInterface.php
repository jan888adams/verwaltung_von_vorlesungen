<?php

namespace verwaltung\factory;

/**
 * Interface InstructorInterface
 */
interface FactoryInterface
{
    /**
     * @param array $attributes
     * @return mixed
     */
    public static function create(array $attributes);

}