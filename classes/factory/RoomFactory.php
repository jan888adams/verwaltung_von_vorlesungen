<?php


namespace verwaltung\factory;


use verwaltung\model\Room;

class RoomFactory implements FactoryInterface
{

    /**
     * @param array $attributes
     * @return Room
     */
    public static function create(array $attributes): Room
    {
        $room = new Room();
        $room->setId($attributes['id']);
        $room->setName($attributes['name']);
        $room->setNumber($attributes['number']);
        return $room;
    }
}