<?php


namespace verwaltung\factory;

use verwaltung\model\User;

class UserFactory implements FactoryInterface
{

    /**
     * @param array $attributes
     * @return mixed
     */
    public static function create(array $attributes): User
    {
        $user = new User();
        $user->setId($attributes['id']);
        $user->setFirstname($attributes['firstname']);
        $user->setSurname($attributes['surname']);
        $user->setEmail($attributes['email']);
        $user->setPassword($attributes['password']);
        $user->setAccessRight($attributes['access_right']);

        return $user;
    }
}