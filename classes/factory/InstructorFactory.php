<?php

namespace verwaltung\factory;

use verwaltung\model\Instructor;

/**
 * Class InstructorFactory
 */
class InstructorFactory implements FactoryInterface
{

    /**
     * @param array $attributes
     * @return Instructor
     */
    public static function create(array $attributes): Instructor
    {
        $instructor = new Instructor();

        $instructor->setId($attributes['id']);
        $instructor->setFirstName($attributes['firstname']);
        $instructor->setSurname($attributes['surname']);
        $instructor->setIsProfessor($attributes['is_professor']);

        return $instructor;
    }
}