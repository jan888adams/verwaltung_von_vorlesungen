<?php


namespace verwaltung\factory;


use verwaltung\model\Student;

class StudentFactory implements FactoryInterface
{

    /**
     * @param array $attributes
     * @return Student
     */
    public static function create(array $attributes): Student
    {
       $student = new Student();
       $student->setId($attributes['id']);
       $student->setFirstname($attributes['firstname']);
       $student->setSurname($attributes['surname']);
       $student->addLectures($attributes['lectures']);

       return $student;
    }
}