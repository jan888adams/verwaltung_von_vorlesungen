<?php

namespace verwaltung\helpers;


use ReflectionClass;
use ReflectionException;


/**
 * Class DependencyResolver
 */
class DependencyResolver
{
    /**
     * @var array
     */
    public static $register = [];

    /**
     * @param $key
     * @param $class
     */
    public static function register($key, $class)
    {
        self::$register[$key] = $class;
    }

    /**
     * @param $key
     * @return mixed
     */
    public static function getClass($key)
    {
        $className = self::$register[$key];
        return new $className;
    }

    /**
     * @param $key
     * @param $dependencies
     * @return object
     */
    public static function getClassWithDependencies($key, $dependencies): object
    {
        $className = self::$register[$key];

        try {
            $class = new ReflectionClass($className);
        } catch (ReflectionException $e) {
            $class = null;
        }

        if (isset($class)) {
            try {
                return $class->newInstanceArgs($dependencies);
            } catch (ReflectionException $e) {
                return $class;
            }
        }
    }
}