<?php


namespace verwaltung\helpers;


class SessionHandler
{

    public static function getSession(): array
    {
        if(!isset($_SESSION['access_right'])){
            $_SESSION['access_right'] = 0;
        }
        return $_SESSION;
    }
}