<?php

namespace verwaltung\helpers;

/**
 * Class Router
 * @package verwaltung\helpers
 */
class Router
{
    /**
     * @var array
     */
    private static $routs = [];

    /**
     * @param string $url
     * @param array $requestActions
     * @return void
     */
    public static function register(string $url, array $requestActions): void
    {
        self::$routs[$url] = $requestActions;
    }

    /**
     * @return array
     */
    public static function getRouts(): array
    {
        return self::$routs;
    }

}