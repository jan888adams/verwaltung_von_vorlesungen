<?php

namespace verwaltung\database;

use mysqli;
use mysqli_result;
use verwaltung\database\DataAccessInterface;

/**
 * Class MySqlConnection
 */
class MySqlConnection implements DataAccessInterface
{
    /**
     * @var string
     */
    private $servername = "db";
    /**
     * @var string
     */
    private $username = "db";
    /**
     * @var string
     */
    private $password = "db";
    /**
     * @var string
     */
    private $database = "db";
    /**
     * @var mysqli
     */
    private $connection;


    /**
     * MySqlConnection constructor.
     */
    public function __construct()
    {
        $this->connection = new mysqli($this->servername, $this->username, $this->password, $this->database);
        if ($this->connection->error) {
            die("Connection failed: " . $this->connection->connect_error);
        }
    }

    /**
     * @param $name
     * @param $condition
     * @param $limit
     * @return array|bool|mysqli_result|null
     */
    public function read($name, $condition = "", $limit = 1)
    {
        $where = "";

        if ($condition != "") {
            foreach ($condition as $conditionKey => $conditionValue) {
                $where .= "`" . $conditionKey . "` = '" . $conditionValue . "'";
            }
        } else {
            $where = "'1' '=' '1'";
        }

        $sql = "SELECT * FROM " . $name . " WHERE " . $where;

        return $this->connection->query($sql)->fetch_all();
    }

    /**
     * @param $name
     * @param $data
     * @return mixed|void
     */
    public function save($name, $data): bool
    {
        $columns = "(";
        $values = "(";

        foreach ($data as $column => $value) {
            $columns .= "`" . $column . "`,";
            $values .= "'" . $value . "',";
        }

        $columns = rtrim($columns, ',');
        $values = rtrim($values, ',');

        $columns .= ")";
        $values .= ")";

        $sql = "INSERT INTO " . $name . " " . $columns . " VALUES " . $values;

        return $this->connection->query($sql);
    }

    /**
     * @param $name
     * @param $condition
     * @param int $limit
     * @return array|null
     */
    public function delete($name, $condition, $limit = 1)
    {
        $where = "";
        foreach ($condition as $conditionKey => $conditionValue) {
            $where .= "`" . $conditionKey . "` = '" . $conditionValue . "'";
        }

        $this->connection->query("DELETE FROM " . $name . " WHERE " . $where);
    }

    public function update($name, $params, $condition)
    {
        $where = "";
        $update = "";
        foreach ($condition as $conditionKey => $conditionValue) {
            $where .= "`" . $conditionKey . "` = '" . $conditionValue . "'";
        }

        foreach ($params as $column => $value) {
            $update .= "`" . $column . "` = '" . $value . "'" . ",";
        }

        $update = rtrim($update, ',');

        $this->connection->query("UPDATE " . $name . " SET " . $update . " WHERE " . $where);
    }
}