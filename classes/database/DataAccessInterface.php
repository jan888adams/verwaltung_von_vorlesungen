<?php

namespace verwaltung\database;

/**
 * Interface DataAccessInterface
 */
interface DataAccessInterface
{
    /**
     * @param $name
     * @param $condition
     * @param $limit
     * @return mixed
     */
    public function read($name, $condition = "", $limit = 1);

    /**
     * @param $name
     * @param $data
     * @return mixed
     */
    public function save($name, $data);

    /**
     * @param $name
     * @param $condition
     * @param int $limit
     * @return mixed
     */
    public function delete($name, $condition, $limit = 1);

    /**
     * @param $name
     * @param $params
     * @param $condition
     * @return mixed
     */
    public function update($name, $params, $condition);

}