<?php


use verwaltung\model\Lecture;

interface ServicePersistenceInterface
{
    public function persist(Lecture $lecture);

}