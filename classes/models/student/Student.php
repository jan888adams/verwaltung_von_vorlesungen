<?php


namespace verwaltung\model;


/**
 * Class Student
 * @package verwaltung\model
 */
class Student
{
    /**
     * @var string
     */
    private $id;
    /**
     * @var string
     */
    private $firstname;
    /**
     * @var string
     */
    private $surname;
    /**
     * @var array
     */
    private $lectures = [];

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return Student
     */
    public function setId(string $id): Student
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstname(): string
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     * @return Student
     */
    public function setFirstname(string $firstname): Student
    {
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     * @return Student
     */
    public function setSurname(string $surname): Student
    {
        $this->surname = $surname;
        return $this;
    }

    /**
     * @return array
     */
    public function getLectures(): array
    {
        return $this->lectures;
    }

    /**
     * @param string $lectures
     * @return Student
     */
    public function addLectures($lectures = []): Student
    {
        $this->lectures = array_merge($this->lectures,  $lectures);
        return $this;
    }

}