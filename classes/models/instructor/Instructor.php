<?php

namespace verwaltung\model;

class Instructor
{

    /**
     * @var string
     */
    private $id;
    /**
     * @var string
     */
    private $firstName;
    /**
     * @var string
     */
    private $surname;
    /**
     * @var bool
     */
    private $isProfessor;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return Instructor
     */
    public function setId(string $id): Instructor
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return Instructor
     */
    public function setFirstName(string $firstName): Instructor
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     * @return Instructor
     */
    public function setSurname(string $surname): Instructor
    {
        $this->surname = $surname;
        return $this;
    }

    /**
     * @return int
     */
    public function isProfessor(): int
    {
        return $this->isProfessor;
    }

    /**
     * @param int $isProfessor
     * @return Instructor
     */
    public function setIsProfessor(int $isProfessor): Instructor
    {
        $this->isProfessor = $isProfessor;
        return $this;
    }

}