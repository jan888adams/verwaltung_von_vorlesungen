<?php

namespace verwaltung\models\lecture;

use verwaltung\factory\FactoryInterface;

/**
 * Class LectureFactory
 */
class LectureFactory implements FactoryInterface
{

    /**
     * @param array $attributes
     * @return Lecture
     */
    public static function create(array $attributes): Lecture
    {
        $lecture = new Lecture();

        $lecture->setId($attributes['id']);
        $lecture->setName($attributes['name']);
        $lecture->setRoom($attributes['room']);
        $lecture->setInstructor($attributes['instructor']);

        return $lecture;
    }
}