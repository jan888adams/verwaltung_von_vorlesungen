<?php


use verwaltung\helpers\DependencyResolver;
use verwaltung\model\Lecture;
use verwaltung\repository\RepositoryInterface;

final class LecturePersistence implements ServicePersistenceInterface
{
    /**
     * @var RepositoryInterface
     */
    private $lectureRepository;

    /**
     * LecturePersistence constructor.
     */
    public function __construct()
    {
        $this->lectureRepository = DependencyResolver::getClass('lecture-repository');
    }

    /**
     * @param Lecture $lecture
     */
    public function persist(Lecture $lecture)
    {
        if(!$this->lectureRepository->save($lecture)){
            throw new LectureNotPersisted('lecture id:' . $lecture->getId());
        }
    }

}