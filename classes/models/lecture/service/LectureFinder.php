<?php


use verwaltung\factory\FactoryInterface;
use verwaltung\helpers\DependencyResolver;
use verwaltung\model\Lecture;
use verwaltung\repository\RepositoryInterface;


final class LectureFinder implements ServiceFinderInterface
{
    /**
     * @var RepositoryInterface
     */
    private $lectureRepository;

    /**
     * @var RepositoryInterface
     */
    private $roomRepository;

    /**
     * @var RepositoryInterface
     */
    private $instructorRepository;

    /**
     * @var FactoryInterface
     */
    private $factory;


    public function __construct()
    {
        $this->lectureRepository = DependencyResolver::getClass('lecture-repository');
        $this->roomRepository = DependencyResolver::getClass('room-repository');
        $this->instructorRepository = DependencyResolver::getClass('instructor-repository');
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function find(string $id): Lecture
    {
        $lectureData = $this->lectureRepository->findById($id);
        $room = $this->roomRepository->findById($lectureData['room_id']);
        $instructor = $this->instructorRepository->findById($lectureData['instructor_id']);

        if ($lectureData === null) {
            throw new LectureNotExist($id);
        }

        return $this->factory::create([
            'id' => $lectureData['id'],
            'name' => $lectureData['name'],
            'room' => $room,
            'instructor' => $instructor
        ]);
    }

    /**
     * @return Lecture[]
     */
    public function findAll(): array
    {
        $allLectureData = $this->lectureRepository->findAll();

        /** @var lecture[] $lectures */
        $lectures = [];

        foreach ($allLectureData as $lectureData) {
            $lectures[] = $this->find($lectureData);
        }

        return $lectures;
    }


}