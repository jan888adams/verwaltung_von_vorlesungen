<?php

namespace verwaltung\models\lecture;

use verwaltung\database\DataAccessInterface;
use verwaltung\factory\FactoryInterface;
use verwaltung\repository\RepositoryInterface;


/**
 * Class LectureRepository
 */
class LectureRepository implements RepositoryInterface
{
    /**
     * @var DataAccessInterface
     */
    private $dataAccess;
    /**
     * @var FactoryInterface
     */
    private $factory;

    /**
     * InstructorRepository constructor.
     * @param DataAccessInterface $dataAccess
     * @param FactoryInterface $factory
     */
    public function __construct(DataAccessInterface $dataAccess, FactoryInterface $factory)
    {
        $this->dataAccess = $dataAccess;
        $this->factory = $factory;
    }

    /**
     * @param $model
     */
    public function save($model): bool
    {
       return $this->dataAccess->save("lecture", [
            'id' => $model->getId(),
            'name' => $model->getName(),
            'room_id' => $model->getRoom()->getId(),
            'instructor_id' => $model->getInstructor()->getId(),
        ]);
    }

    /**
     * @param $id
     * @return array
     */
    public function findById($id): array
    {
        $row =  $this->dataAccess->read('lecture', ['id' => $id]);

        return [
            'id' => $row[0][0],
            'name' => $row[0][1],
            'room_id' => $row[0][2],
            'instructor_id' => $row[0][3]
        ];

    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        $result = $this->dataAccess->read('lecture');

        foreach ($result as $row) {
            $allLectures[] = $this->factory::create([
                'id' => $row[0],
                'name' => $row[1],
            ]);
        }

        return $result;
    }


    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function update($model)
    {
        // TODO: Implement update() method.
    }

    public function softDelete($model)
    {
        // TODO: Implement softDelete() method.
    }
}