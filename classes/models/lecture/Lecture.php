<?php

namespace verwaltung\models\lecture;

/**
 * Class lecture
 */
class Lecture
{
    /**
     * @var string
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var Room
     */
    private $room;

    /**
     * @return Room
     */
    public function getRoom(): Room
    {
        return $this->room;
    }

    /**
     * @param Room $room
     * @return Lecture
     */
    public function setRoom(Room $room): Lecture
    {
        $this->room = $room;
        return $this;
    }

    /**
     * @return Instructor
     */
    public function getInstructor(): Instructor
    {
        return $this->instructor;
    }

    /**
     * @param Instructor $instructor
     * @return Lecture
     */
    public function setInstructor(Instructor $instructor): Lecture
    {
        $this->instructor = $instructor;
        return $this;
    }
    /**
     * @var Instructor
     */
    private $instructor;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return Lecture
     */
    public function setId(string $id): Lecture
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Lecture
     */
    public function setName(string $name): Lecture
    {
        $this->name = $name;
        return $this;
    }

}