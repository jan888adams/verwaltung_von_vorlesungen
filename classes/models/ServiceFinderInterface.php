<?php


interface ServiceFinderInterface
{
    /**
     * @param string $id
     * @return mixed
     */
    public function find(string $id);

    /**
     * @return mixed
     */
    public function findAll();

}