<?php

namespace verwaltung\repository;

use verwaltung\database\DataAccessInterface;
use verwaltung\factory\FactoryInterface;


/**
 * Class InstructorRepository
 */
class InstructorRepository implements RepositoryInterface
{
    /**
     * @var DataAccessInterface
     */
    private $dataAccess;
    /**
     * @var FactoryInterface
     */
    private $factory;

    /**
     * InstructorRepository constructor.
     * @param DataAccessInterface $dataAccess
     * @param FactoryInterface $factory
     */
    public function __construct(DataAccessInterface $dataAccess, FactoryInterface $factory)
    {
        $this->dataAccess = $dataAccess;
        $this->factory = $factory;
    }

    /**
     * @param $model
     * @return mixed|void
     */
    public function save($model)
    {
        $this->dataAccess->save("instructor", [
            'id' => $model->getId(),
            'firstname' => $model->getFirstname(),
            'surname' => $model->getSurname(),
            'is_professor' => $model->isProfessor()
        ]);
    }

    /**
     * @param $id
     * @return mixed|void
     */
    public function findById($id)
    {
        $attributes = $this->dataAccess->read('instructor', ['id' => $id]);

        return $this->factory::create([
            'id' => $attributes[0][0],
            'firstname' => $attributes[0][1],
            'surname' => $attributes[0][2],
            'is_professor' => $attributes[0][3]
        ]);
    }

    /**
     * @return mixed|void
     */
    public function findAll(): array
    {
        $allInstructors = [];
        $allInstructorData = $this->dataAccess->read('instructor');

        foreach ($allInstructorData as $attributes) {
            $allInstructors[] = $this->factory::create([
                'id' => $attributes[0],
                'firstname' => $attributes[1],
                'surname' => $attributes[2],
                'is_professor' => $attributes[3]
            ]);
        }

        return $allInstructors;
    }

    /**
     * @param $id
     * @return mixed|void
     */
    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    /**
     * @param $model
     * @return mixed|void
     */
    public function update($model)
    {
        // TODO: Implement update() method.
    }

    /**
     * @param $model
     * @return mixed|void
     */
    public function softDelete($model)
    {
        // TODO: Implement softDelete() method.
    }
}