<?php


namespace verwaltung\repository;

use verwaltung\database\DataAccessInterface;
use verwaltung\factory\FactoryInterface;
use verwaltung\model\User;

class UserRepository implements RepositoryInterface
{
    /**
     * @var DataAccessInterface
     */
    private $dataAccess;
    /**
     * @var FactoryInterface
     */
    private $factory;

    /**
     * RoomRepository constructor.
     * @param DataAccessInterface $dataAccess
     * @param FactoryInterface $factory
     */
    public function __construct(DataAccessInterface $dataAccess, FactoryInterface $factory)
    {
        $this->dataAccess = $dataAccess;
        $this->factory = $factory;
    }

    /**
     * @param $model
     * @return mixed
     */
    public function save($model)
    {
        $this->dataAccess->save('user',
            [
                'id' => $model->getId(),
                'firstname' => $model->getFirstname(),
                'surname' => $model->getSurname(),
                'email' => $model->getEmail(),
                'password' => $model->getPassword(),
                'access_right' => $model->getAccessRight()
            ]);
    }

    public function findByEmail($email): ?User
    {
        $attributes = $this->dataAccess->read('user', [
            'email' => $email
        ]);

        if($attributes != null){
            return $this->factory->create([
                'id' => $attributes[0][0],
                'firstname' => $attributes[0][1],
                'surname' => $attributes[0][2],
                'email' => $attributes[0][3],
                'password' => $attributes[0][4],
                'access_right' => $attributes[0][5]
            ]);
        }

        return null;

    }

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id)
    {
        // TODO: Implement findById() method.
    }

    /**
     * @return mixed
     */
    public function findAll()
    {
        // TODO: Implement findAll() method.
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    /**
     * @param $model
     * @return mixed
     */
    public function update($model)
    {
        // TODO: Implement update() method.
    }

    /**
     * @param $model
     * @return mixed
     */
    public function softDelete($model)
    {
        // TODO: Implement softDelete() method.
    }
}