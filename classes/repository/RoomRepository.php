<?php

namespace verwaltung\repository;

use verwaltung\database\DataAccessInterface;
use verwaltung\factory\FactoryInterface;

/**
 * Class RoomRepository
 * @package verwaltung\repository
 */
class RoomRepository implements RepositoryInterface
{
    /**
     * @var DataAccessInterface
     */
    private $dataAccess;
    /**
     * @var FactoryInterface
     */
    private $factory;

    /**
     * RoomRepository constructor.
     * @param DataAccessInterface $dataAccess
     * @param FactoryInterface $factory
     */
    public function __construct(DataAccessInterface $dataAccess, FactoryInterface $factory)
    {
        $this->dataAccess = $dataAccess;
        $this->factory = $factory;
    }

    /**
     * @param $model
     * @return mixed
     */
    public function save($model): void
    {
        $this->dataAccess->save("room", [
            'id' => $model->getId(),
            'name' => $model->getName(),
            'number' => $model->getNumber(),
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id)
    {
        $attributes = $this->dataAccess->read('room', ['id' => $id]);
        return $this->factory::create([
            'id' => $attributes[0][0],
            'name' => $attributes[0][1],
            'number' => $attributes[0][2]
        ]);
    }

    /**
     * @return mixed
     */
    public function findAll(): array
    {
        $allRooms = [];

        $allRoomData = $this->dataAccess->read('room');

        foreach ($allRoomData as $attributes) {
            $allRooms[] = $this->factory::create([
                'id' => $attributes[0],
                'name' => $attributes[1],
                'number' => $attributes[2],
            ]);
        }

        return $allRooms;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    /**
     * @param $model
     * @return mixed
     */
    public function update($model)
    {
        // TODO: Implement update() method.
    }

    /**
     * @param $model
     * @return mixed
     */
    public function softDelete($model)
    {
        // TODO: Implement softDelete() method.
    }
}