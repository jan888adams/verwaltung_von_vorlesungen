<?php


namespace verwaltung\repository;


use verwaltung\database\DataAccessInterface;
use verwaltung\factory\FactoryInterface;

class StudentRepository implements RepositoryInterface
{
    /**
     * @var DataAccessInterface
     */
    private $dataAccess;
    /**
     * @var FactoryInterface
     */
    private $factory;

    /**
     * RoomRepository constructor.
     * @param DataAccessInterface $dataAccess
     * @param FactoryInterface $factory
     */
    public function __construct(DataAccessInterface $dataAccess, FactoryInterface $factory)
    {
        $this->dataAccess = $dataAccess;
        $this->factory = $factory;
    }

    /**
     * @param $model
     * @return mixed
     */
    public function save($model)
    {
        $this->dataAccess->save('student', [
            'id' => $model->getId(),
            'firstname' => $model->getFirstname(),
            'surname' => $model->getSurname()
        ]);

        foreach ($model->getLectures() as $lecture) {
            $this->dataAccess->save('studentToLecture', [
                'student_id' => $model->getStudentId(),
                'lecture_id' => $lecture->getId()
            ]);
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id)
    {
        $studentData = $this->dataAccess->read('student', ['id' => $id]);
        $lectureData = $this->dataAccess->read('studentToLecture', ['student_id' => $id]);

        $lectureAttributes = [];

        if ($lectureData != []) {
            foreach ($lectureData[0] as $value) {
                $lectureAttributes[] = $value;
            }
        }

        if (!$studentData) {
            return null;
        }

        return $this->factory->create([
            'id' => $studentData[0][0],
            'firstname' => $studentData[0][1],
            'surname' => $studentData[0][2],
            'lectures' => $lectureAttributes
        ]);
    }

    /**
     * @return mixed
     */
    public function findAll()
    {
        // TODO: Implement findAll() method.
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    /**
     * @param $model
     * @return mixed
     */
    public function update($model)
    {
        $this->dataAccess->update('student', [
            'id' => $model->getId(),
            'firstname' => $model->getFirstname(),
            'surname' => $model->getSurname()
        ], ['id' => $model->getId()]);

        foreach ($model->getLectures() as $lecture) {
            $this->dataAccess->update('studentToLecture', [
                'student_id' => $model->getStudentId(),
                'lecture_id' => $lecture->getId()
            ], ['id' => $model->getId()]);
        }
    }

    public function saveLecture($student, $lecture)
    {
        $this->dataAccess->save('studentToLecture', [
            'student_id' => $student->getId(),
            'lecture_id' => $lecture->getId()
        ]);
    }

    /**
     * @param $model
     * @return mixed
     */
    public
    function softDelete(
        $model
    ) {
        // TODO: Implement softDelete() method.
    }
}