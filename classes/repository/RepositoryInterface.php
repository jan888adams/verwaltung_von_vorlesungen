<?php

namespace verwaltung\repository;

/**
 * Interface RepositoryInterface
 */
interface RepositoryInterface
{
    /**
     * @param $model
     * @return mixed
     */
    public function save($model);

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id);

    /**
     * @return mixed
     */
    public function findAll();

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * @param $model
     * @return mixed
     */
    public function update($model);

    /**
     * @param $model
     * @return mixed
     */
    public function softDelete($model);

}