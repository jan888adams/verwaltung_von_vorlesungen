<?php
use verwaltung\helpers\DependencyResolver;

DependencyResolver::register('db-connection', 'verwaltung\database\MySqlConnection');

DependencyResolver::register('instructor-repository', 'verwaltung\repository\InstructorRepository');
DependencyResolver::register('lecture-repository', 'verwaltung\models\lecture\LectureRepository');
DependencyResolver::register('room-repository', 'verwaltung\repository\RoomRepository');
DependencyResolver::register('user-repository', 'verwaltung\repository\UserRepository');
DependencyResolver::register('student-repository', 'verwaltung\repository\StudentRepository');

DependencyResolver::register('instructor-factory', 'verwaltung\factory\InstructorFactory');
DependencyResolver::register('lecture-factory', 'verwaltung\models\lecture\LectureFactory');
DependencyResolver::register('room-factory', 'verwaltung\factory\RoomFactory');
DependencyResolver::register('user-factory', 'verwaltung\factory\UserFactory');
DependencyResolver::register('student-factory', 'verwaltung\factory\StudentFactory');

DependencyResolver::register('instructor-controller', 'verwaltung\controller\InstructorController');
DependencyResolver::register('lecture-controller', 'verwaltung\controller\LectureController');
DependencyResolver::register('room-controller', 'verwaltung\controller\RoomController');
DependencyResolver::register('user-controller', 'verwaltung\controller\UserController');
DependencyResolver::register('student-controller', 'verwaltung\controller\StudentController');



