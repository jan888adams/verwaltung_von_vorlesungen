<?php

require_once 'vendor/autoload.php';

use verwaltung\helpers\Router;
use verwaltung\controller\FrontController;

session_start();

include_once('config.php');
include_once ('routs.php');

$frontController = new FrontController(Router::getRouts());
$frontController->run();

