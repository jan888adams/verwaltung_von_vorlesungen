<?php

use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use verwaltung\helpers\DependencyResolver;
use verwaltung\helpers\Router;

$connection = DependencyResolver::getClass('db-connection');

$instructorFactory = DependencyResolver::getClass('instructor-factory');
$lectureFactory = DependencyResolver::getClass('lecture-factory');
$roomFactory = DependencyResolver::getClass('room-factory');
$userFactory = DependencyResolver::getClass('user-factory');
$studentFactory = DependencyResolver::getClass('student-factory');

$instructorRepository = DependencyResolver
    ::getClassWithDependencies('instructor-repository', [$connection, $instructorFactory]);
$lectureRepository = DependencyResolver
    ::getClassWithDependencies('lecture-repository', [$connection, $lectureFactory]);
$roomRepository = DependencyResolver
    ::getClassWithDependencies('room-repository', [$connection, $roomFactory]);
$userRepository = DependencyResolver
    ::getClassWithDependencies('user-repository', [$connection, $userFactory]);
$studentRepository = DependencyResolver
    ::getClassWithDependencies('student-repository', [$connection, $studentFactory]);


$twig = new Environment(new FilesystemLoader('templates'));

Router::register("/overview", [
    'http' => [
        'controller' => 'verwaltung\controller\LectureController',
        'dependencies' => [$lectureRepository, $lectureFactory, $twig],
        'action' => 'showTable'
    ],

    'post' => [
        'controller' => 'verwaltung\controller\StudentController',
        'dependencies' => [$studentRepository, $studentFactory, $twig],
        'action' => 'registerToLecture'
    ]
]);

Router::register("/showInstructors", [
    'http' => [
        'controller' => 'verwaltung\controller\InstructorController',
        'dependencies' => [$instructorRepository, $instructorFactory, $twig],
        'action' => 'showTable'
    ],
]);

Router::register("/instructor", [
    'get' => [
        'controller' => 'verwaltung\controller\InstructorController',
        'dependencies' => [$instructorRepository, $instructorFactory, $twig],
        'action' => 'showInstructor'
    ],
]);

Router::register("/addInstructor", [
    'http' => [
        'controller' => 'verwaltung\controller\InstructorController',
        'dependencies' => [$instructorRepository, $instructorFactory, $twig],
        'action' => 'showForm'
    ],

    'post' => [
        'controller' => 'verwaltung\controller\InstructorController',
        'dependencies' => [$instructorRepository, $instructorFactory, $twig],
        'action' => 'addInstructor'
    ]
]);

Router::register("/addLecture", [
    'http' => [
        'controller' => 'verwaltung\controller\LectureController',
        'dependencies' => [$lectureRepository, $lectureFactory, $twig],
        'action' => 'showForm'
    ],

    'post' => [
        'controller' => 'verwaltung\controller\LectureController',
        'dependencies' => [$lectureRepository, $lectureFactory, $twig],
        'action' => 'addLecture',
        'params' => [$_POST]
    ]
]);

Router::register("/addRoom", [
    'http' => [
        'controller' => 'verwaltung\controller\RoomController',
        'dependencies' => [$roomRepository, $roomFactory, $twig],
        'action' => 'showForm',
    ],

    'post' => [
        'controller' => 'verwaltung\controller\RoomController',
        'dependencies' => [$roomRepository, $roomFactory, $twig],
        'action' => 'addRoom',
        'params' => [$_POST]
    ],

]);

Router::register('/registration', [
    'http' => [
        'controller' => 'verwaltung\controller\UserController',
        'dependencies' => [$userRepository, $userFactory, $twig],
        'action' => 'showForm'
    ],

    'post' => [
        'controller' => 'verwaltung\controller\UserController',
        'dependencies' => [$userRepository, $userFactory, $twig],
        'action' => 'addUser'
    ]
]);

Router::register('/', [

    'http' => [
        'controller' => 'verwaltung\controller\LoginController',
        'dependencies' => [$userRepository, $twig],
        'action' => 'showForm'
    ],

    'post' => [
        'controller' => 'verwaltung\controller\LoginController',
        'dependencies' => [$userRepository, $twig],
        'action' => 'login'
    ]
]);

Router::register('/logout', [
    'http' => [
        'controller' => 'verwaltung\controller\LoginController',
        'dependencies' => [$userRepository, $twig],
        'action' => 'logout'
    ]
]);